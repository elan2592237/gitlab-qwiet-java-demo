#!/bin/sh

#### Analyze code
BASEDIR=$(dirname $0)
echo "Script location: ${CI_PROJECT_DIR}"

SHIFTLEFT_SBOM_GENERATOR=2 sl analyze \
  --force \
  --app "$CI_PROJECT_NAME" \
  --oss-project-dir "$CI_PROJECT_DIR" \
  --tag branch="$CI_COMMIT_REF_NAME" \
  --wait \
  .

# Run check-analysis and save report to /tmp/check-analysis.md
sl check-analysis \
  --v2 \
  --app "$CI_PROJECT_NAME" \
  --report \
  --report-file check-analysis.md \
  --source "tag.branch=master" \
  --target "tag.branch=$CI_COMMIT_REF_NAME"

exit 0
